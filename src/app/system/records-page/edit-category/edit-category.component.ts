import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Category} from '../../shared/models/category.model';
import {CategoriesService} from '../../shared/services/categories.service';
import {Message} from '../../../shared/models/message.model';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'wfm-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit, OnDestroy {

  sub1: Subscription;
  @Input() categories: Category[] = [];
  @Output() onCategoryEdit = new EventEmitter<Category>();
  currentCategoryID = 1;
  currentCategory: Category;
  message: Message;

  constructor(private categoriesService: CategoriesService) {
  }

  ngOnInit() {
    this.onCategoryChange();
    this.message = new Message('success', '');
  }

  onSubmit(form: NgForm) {
    const {name} = form.value;
    let {capacity} = form.value;
    if (capacity < 0) capacity *= -1;

    const category = new Category(name, capacity, +this.currentCategoryID);

    this.sub1 = this.categoriesService.updateCategory(category)
      .subscribe((category: Category) => {
        this.onCategoryEdit.emit(category);
        this.message.text = 'Категория успешно отредактированна';
        window.setTimeout(() => {
          this.message.text = '';
        }, 5000);
      });
  }

  onCategoryChange() {
    console.log(this.currentCategoryID);
    this.currentCategory = this.categories
      .find(c => c.id === +this.currentCategoryID);
  }

  ngOnDestroy() {
    if(this.sub1)this.sub1.unsubscribe();
  }
}
