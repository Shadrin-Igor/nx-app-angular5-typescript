import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {BillService} from '../shared/services/bill.service';
import {CategoriesService} from '../shared/services/categories.service';
import {EventsService} from '../shared/services/events.service';
import {BillModel} from '../shared/models/bill.model';
import {Category} from '../shared/models/category.model';
import {WFMEvent} from '../shared/models/event.model';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'wfm-planning-page',
  templateUrl: './planning-page.component.html',
  styleUrls: ['./planning-page.component.scss']
})
export class PlanningPageComponent implements OnInit, OnDestroy {

  isLoaded = false;
  bill: BillModel;
  categories: Category[] = [];
  events: WFMEvent[] = [];
  sub1: Subscription;

  constructor(private billService: BillService,
              private categoriesService: CategoriesService,
              private eventService: EventsService) {
  }

  ngOnInit() {
    this.sub1 = Observable.combineLatest(
      this.billService.getBill(),
      this.categoriesService.getCategory(),
      this.eventService.getEvents(),
    )
      .subscribe((data: [BillModel, Category[], WFMEvent[]]) => {
        this.bill = data[0];
        this.categories = data[1];
        this.events = data[2];
        this.isLoaded = true;
      });
  }

  getCategoryCost(cat: Category): number {
    let cost = 0;

    const events = this.events.filter(item => item.category === cat.id && item.type === 'outcome');
    return events.reduce((total, e) => {
      total += e.amount;
      return total;
    }, 0);
  }

  private getPercent(cat: Category): number {
    const percent = (100 * this.getCategoryCost(cat) / cat.capacity).toFixed(2);
    return percent > 100 ? 100 : percent;
  }

  getCatPercent(cat: Category): string {
    return `${this.getPercent(cat)}%`;
  }

  getCatColorClass(cat: Category): string {
    const percent = this.getPercent(cat);
    return percent === 100 ? 'danger' : percent > 30 ? 'warning' : 'success';
  }

  ngOnDestroy() {
    if (this.sub1) this.sub1.unsubscribe();
  }

}
