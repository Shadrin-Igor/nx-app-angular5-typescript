import {Component, Input, OnInit} from '@angular/core';
import {Category} from '../../shared/models/category.model';
import {WFMEvent} from '../../shared/models/event.model';

@Component({
  selector: 'wfm-history-events',
  templateUrl: './history-events.component.html',
  styleUrls: ['./history-events.component.scss']
})
export class HistoryEventsComponent implements OnInit {

  @Input() categories: Category[] = [];
  @Input() events: WFMEvent[] = [];
  findText = '';
  searchValue: WFMEvent[] = [];
  searchPlaceholder = 'Сумма';
  searchField = 'amount';

  constructor() { }

  ngOnInit() {
    this.events.forEach((event) => {
      const findingCategory = this.categories.find(c => c.id === event.category);

      if (findingCategory) {
        event.cat_name = findingCategory.name;
      } else {
        event.cat_name = '---';
      }
    });
  }

  getEventClass(e: WFMEvent) {
    return {
      'label': true,
      'label-success': e.type === 'income',
      'label-danger': e.type === 'outcome'
    };
  }


  changeCriteria(field: string) {
    const namesMap = {
      amount: 'Сумма',
      date: 'Дата',
      category: 'Категория',
      type: 'Тип'
    };

    this.searchPlaceholder = namesMap[field];
    this.searchField = field;
    console.log('this.searchField', this.searchField);
  }

}
