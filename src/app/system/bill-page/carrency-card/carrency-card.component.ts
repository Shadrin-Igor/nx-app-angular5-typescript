import {Component, Input} from '@angular/core';

@Component({
  selector: 'wfm-carrency-card',
  templateUrl: './carrency-card.component.html',
  styleUrls: ['./carrency-card.component.scss']
})
export class CarrencyCardComponent {

  @Input() currency: any;
  currencies: string[] = ['USD', 'EUR'];
  constructor() {}

}
