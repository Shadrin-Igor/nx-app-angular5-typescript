import {Component} from '@angular/core';

@Component({
  selector: 'wfm-loader',
  template: `<div class="loader-animation">Best loader</div>`,
  styleUrls: ['./loader.component.scss']
})

export default class LoaderComponent {}
