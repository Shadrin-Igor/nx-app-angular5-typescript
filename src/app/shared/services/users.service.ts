import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {User} from '../models/user.model';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {BaseApi} from '../core/base-api';

@Injectable()
export class UsersService extends BaseApi {
  handleError;

  constructor(public  http: HttpClient) {
    super(http);
  }

  getUserByEmail(email: string): Observable<User> {
    return this.get(`users?email=${email}`)
      .map((users: User[]) => users[0] ? users[0] : undefined);
  }

  createNewUser(user: User): Observable<User> {
    return this.post(`users`, user);
  }
}
